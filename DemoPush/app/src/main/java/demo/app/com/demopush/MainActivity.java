package demo.app.com.demopush;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URL;

import me.pushy.sdk.Pushy;

public class MainActivity extends AppCompatActivity {

    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv = (TextView) findViewById(R.id.tv);
        Pushy.listen(this);


        new RegisterForPushNotificationsAsync().execute();
    }


    private class RegisterForPushNotificationsAsync extends AsyncTask<Void, Void, String> {
        String registrationID;

        protected String doInBackground(Void... params) {
            try {
                // Ask Pushy to assign a unique registration ID to this device
                registrationID = Pushy.register(getApplicationContext());

                // Send the registration ID to your backend server via an HTTP GET request

                Toast.makeText(MainActivity.this, "registration Id: " + registrationID, Toast.LENGTH_SHORT).show();
                Log.e("PUSH", "registration id: " + registrationID);
//                new URL("https://{YOUR_API_HOSTNAME}/register/device?registration_id=" + registrationID).openConnection();
            } catch (Exception exc) {
                // Return exc to onPostExecute
                return registrationID;
            }

            // Success
            return registrationID;
        }

        @Override
        protected void onPostExecute(String regid) {
            if (regid != null) {
                // Show error as toast message
                Toast.makeText(getApplicationContext(), regid, Toast.LENGTH_LONG).show();
                Log.e("PUSH", "registration id: " + registrationID);
                tv.setText(registrationID);
                return;
            }

            // Succeeded, do something to alert the user
        }
    }


}
